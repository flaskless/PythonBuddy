from setuptools import setup


INSTALL_REQUIRES=[
    "astroid>=2.4.2"
    ,"Flask>=1.1.2"
    ,"isort>=5.6.4"
    ,"lazy-object-proxy>=1.4.3"
    ,"MarkupSafe>=1.1.1"
    ,"mccabe>=0.6.1"
    ,"pycee2 @ git+https://gitlab.com/flaskless/pycee2@master" # pycee from source to be updated with latest changes
    ,"pylint>=2.6.0"
    ,"six>=1.15.0"
    ,"toml>=0.10.2"
    ,"typed-ast>=1.4.1"
    ,"Werkzeug>=1.0.1"
    ,"wrapt>=1.12.1"

]

setup(name='python-buddy',
      version='0.1.0dev0',
      include_package_data=True,
      description='PythonBuddy',
      long_description='Python Online Editor with Assitance',
      long_description_content_type="text/markdown",
      packages=['PythonBuddy'],
      install_requires=INSTALL_REQUIRES,
      python_requires='>=3.6',
      )
